mod cell;
mod parser;
mod utils;

use std::boxed::Box;
use std::vec::Vec;


fn main() {
    let mut input_text = utils::InputText::new("(* 3 4)".to_string());

    let cells: Vec<Box<cell::Cell>> = parser::parse(&mut input_text);

    println!("Success");
    println!("{:?}", cells);
}
