use utils::is_operator;
use std::fmt;
use std::vec::Vec;

#[derive(PartialEq)]
#[derive(Debug)]
pub enum AtomType {
    Symbol,
    Real,
    Sexp,
    String,
    WhiteSpace,
    Unknown,
    Unseful,
}


pub fn compute_atom_type(atom: String) -> AtomType {
    let front = atom.chars().nth(0).unwrap();
    let back = atom.chars().last().unwrap();
    let mut tmp = "".to_string();
    tmp.push(front);
    let is_digit = tmp.parse::<i64>().is_ok();

    if front == '"' &&  back == '"' {
        AtomType::String
    } else if front == ' ' {
        AtomType::WhiteSpace
    } else if is_digit || (is_operator(front) && atom.len() > 1) {
        AtomType::Real
    } else if is_digit && atom.len() == 1 {
        AtomType::Real
    } else {
        AtomType::Symbol
    }
}


pub trait Cell {
    fn to_string(&self) -> String;

    fn get_type(&self) -> AtomType {
        AtomType::Unknown
    }
}

impl fmt::Debug for Cell {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}


pub struct Sexp {
    pub cells: Vec<Box<Cell>>,
}

impl Sexp {
    pub fn new() -> Sexp {
        Sexp{ cells: Vec::new() }
    }
}

impl Cell for Sexp {
    fn to_string(&self) -> String {
        let cell_strings: Vec<String> = self.cells.iter().map(|cell| cell.to_string()).collect();

        return "Sexp(".to_string() + &cell_strings.join(", ") + ")";
    }

    fn get_type(&self) -> AtomType {
        AtomType::Sexp
    }
}


pub trait Atom {
    fn compute_value(&mut self, value: String);
}


pub struct SymbolAtom {
    name: String,
    value: String,
}

impl SymbolAtom {
    pub fn new(name: String) -> SymbolAtom {
        SymbolAtom{name: name, value: "".to_string()}
    }
}

impl Cell for SymbolAtom {
    fn to_string(&self) -> String {
        format!("SymbolAtom(name: {})", &self.name)
    }

    fn get_type(&self) -> AtomType {
        AtomType::Symbol
    }
}

impl Atom for SymbolAtom {
    fn compute_value(&mut self, value: String) {
        self.value = value;
    }
}


pub struct RealAtom {
    value: f64,
    unparsed_value: String,
}

impl RealAtom {
    pub fn new() -> RealAtom {
        RealAtom{ unparsed_value: "".to_string(), value: 0.0 }
    }
}

impl Cell for RealAtom {
    fn to_string(&self) -> String {
        format!("RealAtom(unparsed_value: {}, value: {})", self.unparsed_value, self.value)
    }

    fn get_type(&self) -> AtomType {
        AtomType::Real
    }
}

impl Atom for RealAtom {
    fn compute_value(&mut self, value: String) {
        println!("{}", value);
        self.value = value.parse::<f64>().unwrap();
        self.unparsed_value = value;
    }
}


pub struct StringAtom {
    value: String,
}

impl StringAtom {
    pub fn new() -> StringAtom {
        StringAtom{ value: "".to_string() }
    }
}

impl Cell for StringAtom {
    fn to_string(&self) -> String {
        format!("StringAtom(value: {})", self.value)
    }

    fn get_type(&self) -> AtomType {
        AtomType::String
    }
}

impl Atom for StringAtom {
    fn compute_value(&mut self, value: String) {
        self.value = value;
    }
}


pub struct UnsefulAtom {
    value: String,
}

impl UnsefulAtom {
    pub fn new() -> UnsefulAtom {
        UnsefulAtom{ value: "".to_string() }
    }
}

impl Cell for UnsefulAtom {
    fn to_string(&self) -> String {
        format!("UnsefulAtom(value: {})", self.value)
    }

    fn get_type(&self) -> AtomType {
        AtomType::Unseful
    }
}

impl Atom for UnsefulAtom {
    fn compute_value(&mut self, value: String) {
        self.value = value;
    }
}
