use std::boxed::Box;
use std::vec::Vec;
use cell::Atom;
use cell::AtomType;
use cell::Cell;
use cell::RealAtom;
use cell::StringAtom;
use cell::SymbolAtom;
use cell::Sexp;
use cell::UnsefulAtom;
use cell::compute_atom_type;
use utils::InputText;
use utils::end_sexp;
use utils::is_operator;
use utils::is_whitespace;
use utils::remove_unuseful_atoms;


pub fn parse(input_text: &mut InputText) -> Vec<Box<Cell>> {
    let mut cells: Vec<Box<Cell>> = Vec::new();
    while input_text.has_remaining_chars() {
        let cell = parse_cell(input_text);
        cells.push(cell);
    }

    remove_unuseful_atoms(&mut cells);

    return cells;
}


fn parse_cell(input_text: &mut InputText) -> Box<Cell> {
    let mut ch = input_text.get_next_char();
    let mut quote = Sexp::new();
    let mut using_sexp = false;
    let mut cell: Box<Cell>;

    if ch == '\'' || ch == '`' || ch == ',' {
        using_sexp = true;
        let atom_name = if ch == '\'' {
            "quote".to_string()
        } else if ch == '`' {
            "backquote".to_string()
        } else {
            "comma".to_string()
        };

        let mut atom = SymbolAtom::new(atom_name.clone());
        atom.compute_value(atom_name);
        quote.cells.push(Box::new(atom));

        remove_unuseful_atoms(&mut quote.cells);

        ch = input_text.get_next_char();
    }

    if ch == '(' {
        cell = parse_sexp(input_text).unwrap();
    } else {
        input_text.rewind();
        cell = parse_atom(input_text);
    }

    if using_sexp {
        quote.cells.push(cell);
        cell = Box::new(quote);
    }

    return cell;
}


fn parse_sexp(input_text: &mut InputText) -> Option<Box<Cell>> {
    let mut ch: char;
    let mut sx = Sexp::new();

    while input_text.has_remaining_chars() {
        ch = input_text.get_next_char();
        if ch == ')' {
            remove_unuseful_atoms(&mut sx.cells);
            return Some(Box::new(sx));
        } else {
            input_text.rewind();
            let cell = parse_cell(input_text);
            sx.cells.push(cell);
        }
    }

    return None;
}


fn parse_atom(input_text: &mut InputText) -> Box<Cell> {
    let at: Box<Cell>;
    let ch = input_text.get_next_char();

    if ch == '"' {
        at = parse_string(input_text);
    } else {
        let mut atom = "".to_string();
        atom.push(ch);

        match compute_atom_type(atom.clone()) {
            AtomType::Symbol => {
                let mut cell_atom = SymbolAtom::new(atom.clone());
                cell_atom.compute_value(atom);
                at = Box::new(cell_atom);
            },
            AtomType::Real => {
                input_text.rewind();
                at = parse_real(input_text);
            },
            AtomType::WhiteSpace => {
                let mut cell_atom = UnsefulAtom::new();
                cell_atom.compute_value(atom);
                at = Box::new(cell_atom);
            },
            _ => panic!("error while parsing"),
        };
    }

    return at;
}


fn parse_string(input_text: &mut InputText) -> Box<Cell> {
    let mut string_value = "".to_string();

    while input_text.has_remaining_chars() {
        let ch = input_text.get_next_char();
        if ch == '"' {
            break;
        }
        string_value.push(ch);
    }

    let mut at = StringAtom::new();
    at.compute_value(string_value);

    return Box::new(at);
}


fn parse_real(input_text: &mut InputText) -> Box<Cell> {
    let mut atom = "".to_string();

    loop {
        let ch = input_text.get_next_char();

        if input_text.has_remaining_chars() && !is_operator(ch) && !is_whitespace(ch) && !end_sexp(ch) {
            atom.push(ch);
        } else {
            input_text.rewind();
            break;
        }
    }

    let mut cell_atom = RealAtom::new();
    cell_atom.compute_value(atom);

    return Box::new(cell_atom);
}


#[cfg(test)]
mod tests {
    use utils::InputText;
    use super::*;

    #[test]
    fn parse_simple() {
        let mut input_text = InputText::new("(* 3 4)".to_string());
        let cells = parse(&mut input_text);
        assert_eq!(format!("{:?}", cells), "[Sexp(SymbolAtom(name: *), RealAtom(unparsed_value: 3, value: 3), RealAtom(unparsed_value: 4, value: 4))]");
    }

    #[test]
    fn parse_nested() {
        let mut input_text = InputText::new("(+ (* 3 4) 3)".to_string());
        let cells = parse(&mut input_text);
        assert_eq!(format!("{:?}", cells), "[Sexp(SymbolAtom(name: +), Sexp(SymbolAtom(name: *), RealAtom(unparsed_value: 3, value: 3), RealAtom(unparsed_value: 4, value: 4)), RealAtom(unparsed_value: 3, value: 3))]");
    }

    #[test]
    fn parse_strring() {
        let mut input_text = InputText::new("(+ \"Hello\" \"World\")".to_string());
        let cells = parse(&mut input_text);
        assert_eq!(format!("{:?}", cells), "[Sexp(SymbolAtom(name: +), StringAtom(value: Hello), StringAtom(value: World))]");
    }

    #[test]
    fn parse_multi_digits_number() {
        let mut input_text = InputText::new("(* 30 40)".to_string());
        let cells = parse(&mut input_text);
        assert_eq!(format!("{:?}", cells), "[Sexp(SymbolAtom(name: *), RealAtom(unparsed_value: 30, value: 30), RealAtom(unparsed_value: 40, value: 40))]");
    }

    #[test]
    fn parse_float() {
        let mut input_text = InputText::new("(* 30.36 0.40)".to_string());
        let cells = parse(&mut input_text);
        assert_eq!(format!("{:?}", cells), "[Sexp(SymbolAtom(name: *), RealAtom(unparsed_value: 30.36, value: 30.36), RealAtom(unparsed_value: 0.40, value: 0.4))]");
    }
}
