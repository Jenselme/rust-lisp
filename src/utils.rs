use cell::AtomType;
use cell::Cell;


pub struct InputText {
    text: String,
    pub position: usize,
}


impl InputText {
    pub fn new(text: String) -> InputText {
        InputText {
            text: text,
            position: 0,
        }
    }

    pub fn get_next_char(&mut self) -> char {
        let ch = self.text.chars().nth(self.position).unwrap();
        self.position += 1;
        return ch;
    }

    pub fn rewind(&mut self) {
        self.position -= 1;
    }

    pub fn has_remaining_chars(&self) -> bool {
        return self.position < self.text.len();
    }
}


pub fn remove_unuseful_atoms(cells: &mut Vec<Box<Cell>>) {
    cells.retain(|cell| match cell.get_type() {
        AtomType::Unseful => false,
        _ => true,
    })
}


pub fn is_operator(ch: char) -> bool {
    let operators = "+*-/!=<>\"";
    return operators.find(ch).is_some();
}


pub fn is_whitespace(ch: char) -> bool {
    ch == ' '
}

pub fn end_sexp(ch: char) -> bool {
    ch == ')'
}
